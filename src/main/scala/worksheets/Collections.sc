/**
 * Companion object for Class Date
 */
object Date {
  var numDates: Int = 0

  def apply(dd: Int, mm: Int, yy: Int) = {
    numDates = numDates + 1
    new Date(dd, mm, yy)
  }
}


/**
 * This is how you write a Scala-Doc
 * Class to manage date, month, and year given by user
 * @param dd Date
 * @param mm Month
 * @param yy User
 */
class Date(dd: Int, mm: Int, yy: Int) {
  // Methods and contents of this class
}

// Create instances of Date class by
// using the companion object
val date = Date(17, 11, 2001)
val date2 = Date(20, 4, 2010)

// Get the number of instances from companion object
Date.numDates

// Readable data to mimic
// line by line text file.
val file: Array[String] = Array(
  "Hello world",
  "This is a text file",
  "And something goes here",
  "This is a text file"
)

// Split the lines into words and flatten them into
// a single collection
val words = file flatMap (line => line.split(" "))

// Get the unique set of words from all possible words
val distWords = words distinct

// Map all possible words to the number of times they take place
distWords map {
  word =>
    var times = 0
    words.foreach {
      w => if(w == word) times = times + 1
    }
    (word, times)
}
