package sparkbasics

import org.apache.spark.rdd.RDD

object WorkingRDDs extends App {

  // Read text files from the disk
  val file: RDD[String] = sc.textFile("myFile.txt")
  val sbtFile: RDD[String] = sc.textFile("build.sbt").cache()

  // Merging two RDDs into one
  val mergeSBT: RDD[String] = file ++ sbtFile

  // Get the first element of the RDD
  // (useful for interactive shell)
  val firstElement = mergeSBT.first


  // Running some operation asynchronously on every collection in every partition
  mergeSBT.foreachPartition{
    element =>
      val e: Iterator[String] = element
      e foreach(println(_))
  }

  // Running some operation on every element in an RDD
  file foreach(println(_))

  // Filter operation on an RDD
  val nonEmptyLines: RDD[String] = mergeSBT.filter(_ != "")

  // Set operations on an RDD
  val unionSBT = file union sbtFile
  val intersectSBT = file intersection sbtFile

  // Zip operations on an RDD
  val zipRDD: RDD[(String, String)] = file zip sbtFile

  // For expressions
  val anotherzip: List[(Int, Int)] = for {
    rdd1 <- List(1, 2, 3, 4)
    rdd2 <- List(4, 5, 6, 7)
  } yield(rdd1, rdd2)

  /** *
    * Note that zip operations is always better than manual zipping using for expression
    *
    * Reason: RDDs are asynchronous in nature and thus the results will be unexpected objects
    */

  val something: Unit = for {
    rdd1 <- file
    rdd2 <- sbtFile
  } {
    // Some opearations
  }

}
