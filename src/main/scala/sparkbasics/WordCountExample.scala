package sparkbasics

import org.apache.spark.rdd.RDD

object WordCountExample extends App {

  // Read the text file from the disk
  val file: RDD[String] = sc.textFile("/Users/chetan/Projects/Tuts/myFile.txt")

  // Split the liens into words and flatten them
  val words: RDD[String] = file.flatMap(line => line.split(" "))

  // Count the words by summing up unit values for every word as key
  val count: RDD[(String, Int)] = words.map(w => (w, 1)).reduceByKey(_ + _)

  // Example: Gruoping data without reducing it
  val gorupedData: RDD[(String, Iterable[Int])] = words.map(w => (w, 1)).groupByKey

  // Collect the result into usable value
  val result: Array[(String, Int)] = count.collect()

  // Print the result for every tuple in the collection
  // Notice that we're not using . operator for calling function of object 'result'
  result foreach {
    element =>
      println(element._1 + " occurs " + element._2 + " times.")
  }

  // Terminate the spark instance before exiting the program to perform a clean shutdown
  sc.stop()

  // Perform a clean shutdown by giving out an exit status 0 [optional since the program will exit automatically
  // after all operations have been finished].
  System exit 0 // Note: same as System.exit(0)

}
